﻿About:
This small console app allows a user to surround any data on the clipboard with SQL quotes.

Example:
c2ee540a-d257-476e-a575-ba1c05e01999
6d2fc4d2-fa00-44d3-84d8-2b8c0ac426c5
c1b1d154-dafa-4a23-aaeb-687d6e4d55c9
ae702bc5-5ff3-4386-adfe-f523af2e730d
3d84793c-9cd1-48bb-b3d1-a4c456996a2c

Becomes:
'c2ee540a-d257-476e-a575-ba1c05e01999',
'6d2fc4d2-fa00-44d3-84d8-2b8c0ac426c5',
'c1b1d154-dafa-4a23-aaeb-687d6e4d55c9',
'ae702bc5-5ff3-4386-adfe-f523af2e730d',
'3d84793c-9cd1-48bb-b3d1-a4c456996a2c'

For use in SQL statements such as:
SELECT * FROM Customers WHERE Id IN (
'c2ee540a-d257-476e-a575-ba1c05e01999',
'6d2fc4d2-fa00-44d3-84d8-2b8c0ac426c5',
'c1b1d154-dafa-4a23-aaeb-687d6e4d55c9',
'ae702bc5-5ff3-4386-adfe-f523af2e730d',
'3d84793c-9cd1-48bb-b3d1-a4c456996a2c'
)

Setup:
Create a shortcut to the (.exe) file and assign it a keyboard shortcut such as (CTRL + ALT + M) for quicker use.