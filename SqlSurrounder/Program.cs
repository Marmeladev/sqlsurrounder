﻿using System;
using System.Linq;
using System.Windows;

namespace StringListerApp
{
    class Program
    {
        [STAThread]
        static void Main()
        {
            if (!Clipboard.ContainsText())
                return;

            char[] delimiterChars = { '\r', '\t', '\n' };

            var input = Clipboard.GetText();
            var inputList = input.Split(delimiterChars, int.MaxValue, StringSplitOptions.RemoveEmptyEntries);

            var test = inputList[0].Surround(@"'", @"','");

            for (var i = 0; i < inputList.Length; i++)
                inputList[i] = inputList[i].Surround(@"'", @"',");

            inputList[inputList.Length - 1] = inputList[inputList.Length - 1].Substring(0, inputList[inputList.Length - 1].Length - 1);

            var output = string.Empty;
            inputList.ToList().ForEach(s => output += s + "\r\n");
            Clipboard.SetText(output);
        }
    }

    public static class StringExtensions
    {
        public static string Surround(this string originalText, string startingText, string endingText)
        {
            return startingText + originalText + endingText;
        }
    }
}
